// Aufgabe u10b
// Typkonvertierungen durch Ueberschreiben der Cast-Operatoren
// Philipp Richert
// 13.11.2022

// hab heute keine Zeit den Code zu kommentieren lul

#include "cDreieck.h"
#include "cKreis.h"
#include "cQuadrat.h"

int main() {
	cKreis kreis = cKreis(12.18);
	cQuadrat quadrat = cQuadrat(18.12);
	cDreieck dreieck = cDreieck(23.72);

	kreis.print();
	quadrat.print();
	dreieck.print();

	cout << endl << "Objektzuweisungen mittels Casting:" << endl;

	kreis = (cKreis)quadrat;
	kreis.print();

	quadrat = (cQuadrat)dreieck;
	quadrat.print();

	dreieck = (cDreieck)kreis;
	dreieck.print();

	return 0;
}
#include "cDreieck.h"
// ---
#include "cKreis.h"
#include "cQuadrat.h"

cDreieck::cDreieck(double side_in) {
	seite = side_in;
}

double cDreieck::getSideFromArea(double area) {
	double side = sqrt((area * 4) / sqrt(3));
	return side;
}

cDreieck::operator cKreis() {
	double area = (sqrt(3) * seite * seite) / 4;
	return cKreis(cKreis::getRadiusFromArea(area));
}

cDreieck::operator cQuadrat() {
	double area = (sqrt(3) * seite * seite) / 4;
	return cQuadrat(cQuadrat::getSideFromArea(area));
}

void cDreieck::print() {
	cout << "Seitenlaenge: " << seite << endl;
}
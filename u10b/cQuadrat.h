#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

using namespace std;

class cKreis;
class cDreieck;

class cQuadrat
{
private:
	double seite;
public:
	cQuadrat(double = 1.0);
	static double getSideFromArea(double);
	operator cKreis();
	operator cDreieck();
	void print();
};


#include "cKreis.h"
// ---
#include "cQuadrat.h"
#include "cDreieck.h"

cKreis::cKreis(double rad_in) {
	radius = (rad_in < 0.0) ? 1.0 : rad_in;
}

double cKreis::getRadiusFromArea(double area) {
	double rad = sqrt(area / M_PI);
	return rad;
}

cKreis::operator cDreieck() {
	double area = radius * radius * M_PI;
	return cDreieck(cDreieck::getSideFromArea(area));
}

cKreis::operator cQuadrat() {
	double area = radius * radius * M_PI;
	return cQuadrat(cQuadrat::getSideFromArea(area));
}

void cKreis::print() {
	cout << "Radius: " << radius << endl;
}
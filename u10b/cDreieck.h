#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

using namespace std;

class cKreis;
class cQuadrat;

class cDreieck
{
private:
	double seite;
public:
	cDreieck(double = 1.0);
	static double getSideFromArea(double);
	operator cKreis();
	operator cQuadrat();
	void print();
};


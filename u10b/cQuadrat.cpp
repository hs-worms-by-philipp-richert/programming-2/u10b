#include "cQuadrat.h"
// ---
#include "cKreis.h"
#include "cDreieck.h"

cQuadrat::cQuadrat(double side_in) {
	seite = (side_in < 0) ? 1.0 : side_in;
}

double cQuadrat::getSideFromArea(double area) {
	double side = sqrt(area);
	return side;
}

cQuadrat::operator cDreieck() {
	double area = seite * seite;
	return cDreieck(cDreieck::getSideFromArea(area));
}

cQuadrat::operator cKreis() {
	double area = seite * seite;
	return cKreis(cKreis::getRadiusFromArea(area));
}

void cQuadrat::print() {
	cout << "Kantenlaenge: " << seite << endl;
}
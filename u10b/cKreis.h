#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

using namespace std;

class cQuadrat;
class cDreieck;

class cKreis
{
private:
	double radius;
public:
	cKreis(double = 1.0);
	static double getRadiusFromArea(double);
	operator cQuadrat();
	operator cDreieck();
	void print();
};

